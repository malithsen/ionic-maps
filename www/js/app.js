// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova', 'auth0', 'angular-storage', 'angular-jwt'])

.config(function($stateProvider, $urlRouterProvider, authProvider, $httpProvider,
  jwtInterceptorProvider) {

  $stateProvider
  .state('login', {
    url: '/',
    controller: 'LoginCtrl'
  })
  .state('map', {
    url:'/map',
    templateUrl: 'templates/map.html',
    controller: 'MapCtrl',
    data: {
      requiresLogin: true
    }
  });

  $urlRouterProvider.otherwise("/");

  authProvider.init({
    domain: 'jeggbert.auth0.com',
    clientID: 'W5AKoVlGzHvp600T14pOdaV6VHqjQ0Py',
    loginState: 'login' // This is the name of the state where you'll show the login, which is defined above...
  });

  authProvider.on('loginSuccess', function($state, profilePromise, idToken, store, $rootScope) {
    profilePromise.then(function(profile) {
      store.set('profile', profile);
      store.set('token', idToken);
      $rootScope.lock.hide();
      $state.go('map');
    });
  });

  authProvider.on('authenticated', function($state) {
    $state.go('map');
  });

  jwtInterceptorProvider.tokenGetter = function(store) {
    return store.get('token');
  };

  $httpProvider.interceptors.push('jwtInterceptor');

})

.run(function($ionicPlatform, $rootScope, $state, store, auth, jwtHelper) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

  auth.hookEvents();

  $rootScope.$on('$locationChangeStart', function() {

    var token = store.get('token');
    if (token) {
      if (!jwtHelper.isTokenExpired(token)) {
        if (!auth.isAuthenticated) {
          auth.authenticate(store.get('profile'), token);
        }
      } else {
        $state.go('login');
      }
    }

  });

})

.controller('MapCtrl', function($scope, $state, $rootScope, $cordovaGeolocation, auth, store) {
  console.log("map controller");

  var geoData = [{'name': 'UCSC', 'geo': [6.9029739, 79.8613145], 'description': 'University of Colombo School of Computing'},
                 {'name': 'UoM', 'geo': [6.796861, 79.900673], 'description': 'University of Moratuwa'},
                 {'name': 'Zoo', 'geo': [6.84992,79.880994], 'description': 'National Zoological Gardens'}];

  var options = {timeout: 10000, enableHighAccuracy: true};

  $scope.logout = function(){
    auth.signout();
    store.remove('profile');
    store.remove('token');
    $state.go('login');
  };

  $cordovaGeolocation.getCurrentPosition(options).then(function(curPosition){

    var infoWindow = new google.maps.InfoWindow();

    /*Mark current position*/
    var myPosition = new google.maps.LatLng(curPosition.coords.latitude, curPosition.coords.longitude);

    var mapOptions = {
      center: myPosition,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var marker = new google.maps.Marker({
      icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
      map: $scope.map,
      animation: google.maps.Animation.DROP,
      position: myPosition
    });

    marker.addListener('click', function(){
      infoWindow.setContent('Home marker');
      infoWindow.open($scope.map, marker);
    });

    /*Mark other positions*/
    google.maps.event.addListenerOnce($scope.map, 'idle', function(){
      var position;

      for (var i = 0; i < geoData.length; i++) {
        position = new google.maps.LatLng(geoData[i]['geo'][0], geoData[i]['geo'][1]);

        var marker = new google.maps.Marker({
          position: position,
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          title: geoData[i]['name'],
          info: geoData[i]['description']
        });

        marker.addListener('click', function(){
          infoWindow.setContent("<h3>" + this.title + "</h3> <p>" + this.info + "</p>");
          infoWindow.open($scope.map, this);
        });

      }

    });
  }, function(error){
    console.log("Could not get location");
  });

})
.controller('LoginCtrl', function(store, $scope, $rootScope){

  console.log("login controller");

  $scope.login = function(){

    $rootScope.lock = new Auth0Lock('W5AKoVlGzHvp600T14pOdaV6VHqjQ0Py','jeggbert.auth0.com');

    $rootScope.lock.show({
      connections : ['google-oauth2', 'Username-Password-Authentication'],
    });

    $rootScope.lock.on('signin ready', function() {
      var link = $('<a class="a0-zocial a0-sharepoint" href="#"><span>Login with Foursquare</span></a>');
      link.appendTo('.a0-iconlist');
      link.on('click', function() {
        $rootScope.lock.getClient().login({connection: 'Foursquare'});
      });
    });
  };

  $scope.$on('$ionicView.enter', function(){
    $scope.login();
  });

});
